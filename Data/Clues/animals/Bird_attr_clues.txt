It belongs to the animals category.
You can also call it the Aves.
These are its types: vireo, dominique and black-winged stilt.
Let me give you some more types of it: larid, giant moa and carinate.
It is a chordate.
Let me make it more precise for you! It is a vertebrate.
It has  a wing.
Let me give you one more hint! It also has a second joint.
