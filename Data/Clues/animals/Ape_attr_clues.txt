It belongs to the animals category.
You can also call it the Čovekoliki majmun.
These are the fictional characters related to it - Donkey Kong and Cranky Kong.
These are its scientific names - Hominidae and Gibbon.
These are its types: silverback, chimpanzee and siamang.
Let me give you some more types of it: orangutan, central chimpanzee and gibbon.
It is a mammal.
Let me make it more precise for you! It is a placental.
