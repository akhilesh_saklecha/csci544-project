massive plantigrade carnivorous or omnivorous mammals with long shaggy coats and strong claws
Common characteristics of modern bears include large bodies with stocky legs, long snouts, shaggy hair, plantigrade paws with five nonretractile claws, and short tails.
Bears are mammals of the family Ursidae.
Bears are classified as caniforms, or doglike carnivorans, with the pinnipeds being their closest living relatives.
Bears are found in the continents of North America, South America, Europe, and Asia.
Although there are only eight living species of bear, they are widespread, appearing in a wide variety of habitats throughout the Northern Hemisphere and partially in the Southern Hemisphere.
Bears possess an excellent sense of smell and, despite their heavy build and awkward gait, are adept runners, climbers, and swimmers.
With the exception of courting individuals and mothers with their young, bears are typically solitary animals.
While the polar bear is mostly carnivorous, and the giant panda feeds almost entirely on bamboo, the remaining six species are omnivorous with varied diets.
They are generally diurnal, but may be active during the night or twilight, particularly around humans.
