Old World nocturnal canine mammal closely related to the dog; smaller than a wolf; sometimes hunts in a pack but usually singly or as a member of a pair
Their most common social unit is that of a monogamous pair which defends its territory from other pairs by vigorously chasing intruding rivals and marking landmarks around the territory with their urine and feces.
Although the word jackal has been historically used to refer to many small- to medium-sized species of the wolf genus of mammals, Canis, today it most properly and commonly refers to three species: the black-backed jackal and the side-striped jackal of sub-Saharan Africa, and the golden jackal of northern Africa and south-central Eurasia.
The jackal is a small carnivorous mammal of the genus Canis, which also includes the wolf, dog, and coyote.
Jackals and coyotes are opportunistic omnivores, predators of small- to medium-sized animals and proficient scavengers.
Jackals are crepuscular, most active at dawn and dusk.
The territory may be large enough to hold some young adults which stay with their parents until they establish their own territories.
Their long legs and curved canine teeth are adapted for hunting small mammals, birds, and reptiles, and their large feet and fused leg bones give them a physique well-suited for long-distance running, capable of maintaining speeds of 16 km/h for extended periods of time.
