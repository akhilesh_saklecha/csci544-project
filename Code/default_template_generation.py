import os
import json
import sys
#attributes is an arrya of strings
#word is the word which will be guessed
#function removes invalid words from the attributes array
def filter_values(attributes,word,category):
	rslt = list()
	print("-------------------")
	word = word.lower()
	for attr in attributes:
		if attr != None:
			print (attr +" && "+ word)
		
			if word.strip() in attr.lower() or attr.lower().strip() in word or attr.lower().strip() in category:
				continue
			if "\\" in attr:
				continue
			
			rslt.append(attr)
	return rslt

# def replaceWordWithBlank(currAttr,word):
# 	rslt = list()
# 	for attr in currAttr:
# 		if attr == None:
# 			continue
# 		words = attr.split(" ")
# 		print(words)
# 		newWords = list()
# 		temp = ""
# 		for curword in words:
# 			if curword == word:
# 				newWords.append(curword.replace(curword,'____')
# 		for newWord in newWords:
# 			temp = temp + newWord + " "
# 		rslt.append(temp[:temp[len(temp)-1]])
# 	return rslt
def getDeterminer(word):
	word = word.lower()
	if word[0] == 'a' or word[0] == 'e' or word[0] == 'i' or word[0] == 'o' or word[0] == 'u':
		return ' an '
	else:
		return ' a '


def generateTemplateForSport(freebaseData,wordnetData,word,category):
	sentences = list()
	freebaseAttributes = freebaseData['attributes']
	if wordnetData != None:
		wordnetAttrs = wordnetData['attributes']
	sentences.append("It belongs to the category sports.")
	

	if 'leagues' in freebaseAttributes.keys() and len(freebaseAttributes['leagues'])>0:
	
		# words = replaceWordWithBlank(freebaseAttributes['leagues'],word)
		leagues = filter_values(freebaseAttributes['leagues'],word,category)
		
		if len(leagues)>3:
			if len(leagues)==3:
				sentences.append("These are a few leagues for this game: " + leagues[0] + ", "+ leagues[1]+ " and "+ leagues[2]+ ".")
			else:
				sentences.append("These are a few leagues for this game: " + leagues[0] + ", "+ leagues[1]+ " and "+ leagues[2]+ ".")
				length = len(leagues)
				sent = "Let me give you some more league names "
				if length == 4:
					sent = sent +leagues[length-1] + "."
				elif length == 5:
					sent = sent + leagues[3]+ " and " +leagues[4]+"."
				elif length >5:
					sent = sent + leagues[3]+ ", "+ leagues[4]+ " and "+ leagues[5]+"."
				sentences.append(sent)
		else:
			sent = "The leagues related to this sport: "
			length = len(leagues)
			if length == 1:
				sent = sent + leagues[0]+"."
				sentences.append(sent)
			elif length == 2:
				sent = sent + leagues[0]+ " and " +leagues[1]+"."
				sentences.append(sent)
	
	if 'teams' in freebaseAttributes.keys() and len(freebaseAttributes['teams'])>0:
		print(freebaseAttributes['teams'])
		print(word)
		# words = replaceWordWithBlank(freebaseAttributes['teams'],word)
		teams = filter_values(freebaseAttributes['teams'],word,category)
		
		if len(teams)>3:
			if len(teams)==3:
				sentences.append("The teams in this sport are - " + teams[0] + ", "+ teams[1]+ " and "+ teams[2]+ ".")
			else:
				sentences.append("The teams in this sport are - " + teams[0] + ", "+ teams[1]+ " and "+ teams[2]+ ".")
				length = len(teams)
				sent = "Let me give you some more teams of it: "
				if length == 4:
					sent = sent +teams[length-1] + "."
				elif length == 5:
					sent = sent + teams[3]+ " and " +teams[4]+"."
				elif length >5:
					sent = sent + teams[3]+ ", "+ teams[4]+ " and "+ teams[5]+"."
				sentences.append(sent)
		else:
			sent = "The teams involved in this sport are: "
			length = len(teams)
			if length == 1:
				sent = sent + teams[0]+"."
				sentences.append(sent)
			elif length == 2:
				sent = sent + teams[0]+ " and " +teams[1]+"."
				sentences.append(sent)
	if wordnetData != None:
		hypernyms = filter_values(wordnetAttrs['hypernyms'],word,category)
			
		if len(hypernyms)>0:
			if len(hypernyms)>1:
				sentences.append("It is "+getDeterminer(hypernyms[len(hypernyms)-2]) +hypernyms[len(hypernyms)-2] + ".")
				sentences.append("Let me make it more precise for you! " + "It is a"+ getDeterminer(hypernyms[len(hypernyms)-1])+hypernyms[len(hypernyms)-1]+".")
			else:
				sentences.append("It is also " + getDeterminer(hypernyms[0])+hypernyms[0]+".")
		
		meronyms = filter_values(wordnetAttrs['meronyms'],word,category)
			
		if len(meronyms)>0:
			if len(meronyms)>1:
				sentences.append("One of the terms used in this game is " + meronyms[0] + ".")
				sentences.append("Let me give you one more term used in this game. " + "It is"+meronyms[1]+".")
			else:
				sentences.append("One of the terms used in this game is " +  meronyms[0]+".")
		

		if 'hyponyms' in freebaseAttributes.keys() and len(freebaseAttributes['hyponyms'])>0:
	
			hyponyms = filter_values(freebaseAttributes['hyponyms'],word,category)
			
			if len(hyponyms)>3:
				if len(hyponyms)==3:
					sentences.append("The words similar to this word are " + hyponyms[0] + ", "+ hyponyms[1]+ " and "+ hyponyms[2]+ ".")
				else:
					sentences.append("The words similar to this word are " + hyponyms[0] + ", "+ hyponyms[1]+ " and "+ hyponyms[2]+ ".")
					length = len(hyponyms)
					sent = "Let me give you some more similar words: "
					if length == 4:
						sent = sent +hyponyms[length-1] + "."
					elif length == 5:
						sent = sent + hyponyms[3]+ " and " +hyponyms[4]+"."
					elif length >5:
						sent = sent + hyponyms[3]+ ", "+ hyponyms[4]+ " and "+ hyponyms[5]+"."
					sentences.append(sent)
			else:
				sent = "Let me give you some words similar to this word: "
				length = len(hyponyms)
				if length == 1:
					sent = sent + hyponyms[0]+"."
					sentences.append(sent)
				elif length == 2:
					sent = sent + hyponyms[0]+ " and " +hyponyms[1]+"."
					sentences.append(sent)

	if 'SYN' in freebaseAttributes.keys() and len(freebaseAttributes['SYN'])>0:
	
		# words = replaceWordWithBlank(freebaseAttributes['SYN'],word)
		words = filter_values(freebaseAttributes['SYN'],word,category)
		if len(words)>1:

			sentences.append("It is also know as " + words[0]+" and " + words[1]+".")
		elif len(words) == 1:
			sentences.append("You can also call it the " + words[0]+".")
	
	if 'related_equipment' in freebaseAttributes.keys() and len(freebaseAttributes['related_equipment'])>0:
	
		# words = replaceWordWithBlank(freebaseAttributes['related_equipment'],word)
		words = filter_values(freebaseAttributes['related_equipment'],word,category)
		if len(words)>1:

			sentences.append("The equipment used in this game are " + words[0]+" and " + words[1]+".")
		elif len(words) == 1:
			sentences.append("Some sport related with " + words[0]+".")
	
	return sentences

def generateTemplateForAnimal(freebaseData,wordnetData,word,category):
	sentences = list()
	freebaseAttributes = freebaseData['attributes']
	if wordnetData != None:
		wordnetAttrs = wordnetData['attributes']
	sentences.append("It belongs to the animals category.")
	if 'SYN' in freebaseAttributes.keys() and len(freebaseAttributes['SYN'])>0:
	
		# words = replaceWordWithBlank(freebaseAttributes['SYN'],word)
		words = filter_values(freebaseAttributes['SYN'],word,category)
		if len(words)>1:

			sentences.append("It is also know as " + words[0]+" and " + words[1]+".")
		elif len(words) == 1:
			sentences.append("You can also call it the " + words[0]+".")
	
	
		
	if 'fictional character' in freebaseAttributes.keys() and len(freebaseAttributes['fictional character'])>0:
		# print("---------------------------------")
		# print(freebaseAttributes['fictional character'])
		# words = replaceWordWithBlank(freebaseAttributes['fictional character'],word)
		words = filter_values(freebaseAttributes['fictional character'],word,category)
		if len(words)>1:

			sentences.append("These are the fictional characters related to it - " + words[0]+" and " + words[1]+".")
		elif len(words) == 1:
			sentences.append("The fictional character related to it is " + words[0]+".")
	
		
	if 'scientific names' in freebaseAttributes.keys() and len(freebaseAttributes['scientific names'])>0:
	
		# words = replaceWordWithBlank(freebaseAttributes['scientific names'],word)
		words = filter_values(freebaseAttributes['scientific names'],word,category)
		if len(words)>1:

			sentences.append("These are its scientific names - " + words[0]+" and " + words[1]+".")
		elif len(words) == 1:
			sentences.append("Its scientific name is " + words[0]+".")
	
	if wordnetData != None:
		# hyponyms = replaceWordWithBlank(wordnetAttrs['hyponyms'],word)
		hyponyms = filter_values(wordnetAttrs['hyponyms'],word,category)
		if len(hyponyms)>0:	
			if len(hyponyms)>3:
				if len(hyponyms)==3:
					sentences.append("These are the words that belong to this category - " + hyponyms[0] + ", "+ hyponyms[1]+ " and "+ hyponyms[2]+ ".")
				else:
					sentences.append("These are its types: " + hyponyms[0] + ", "+ hyponyms[1]+ " and "+ hyponyms[2]+ ".")
					length = len(hyponyms)
					sent = "Let me give you some more types of it: "
					
					if length == 4:
						sent = sent +hyponyms[length-1] + "."
					elif length == 5:
						sent = sent + hyponyms[3]+ " and " +hyponyms[4]+"."
					elif length >5:
						sent = sent + hyponyms[3]+ ", "+ hyponyms[4]+ " and "+ hyponyms[5]+"."
					sentences.append(sent)
			else:
				sent = "It can be classified into: "
				length = len(hyponyms)
				if length == 1:
					sent = sent + hyponyms[0]+"."
					sentences.append(sent)
				elif length == 2:
					sent = sent + hyponyms[0]+ " and " +hyponyms[1]+"."
					sentences.append(sent)
		
		# hypernyms = replaceWordWithBlank(wordnetAttrs['hypernyms'],word)
		hypernyms = filter_values(wordnetAttrs['hypernyms'],word,category)
			
		if len(hypernyms)>0:
			if len(hypernyms)>1:
				sentences.append("It is"+getDeterminer(hypernyms[0]) + hypernyms[0] + ".")
				sentences.append("Let me make it more precise for you! " + "It is"+ getDeterminer(hypernyms[1]) + hypernyms[1]+".")
			else:
				sentences.append("It is" + getDeterminer(hypernyms[0])+ hypernyms[0]+".")
		
		# meronyms = replaceWordWithBlank(wordnetAttrs['meronyms'],word)
		meronyms = filter_values(wordnetAttrs['meronyms'],word,category)
		if len(meronyms)>0:
			if len(meronyms)>1:
				sentences.append("It has "+getDeterminer(meronyms[0]) + meronyms[0] + ".")
				sentences.append("Let me give you one more hint! " + "It also has"+ getDeterminer(meronyms[1]) + meronyms[1]+".")
			elif len(meronyms) == 1:
				sentences.append("It has" + getDeterminer(meronyms[0])+ meronyms[0]+".")
	
	return sentences
		
def generateTemplateForFilm(freebaseData,wordnetData,word,category):
	sentences = list()
	freebaseAttributes = freebaseData['attributes']
	# print(freebaseAttributes)
	sentences.append("It is a film.")
	if 'language' in freebaseAttributes.keys() and len(freebaseAttributes['language'])>0:
			sentences.append("It is in "+ str(freebaseAttributes['language'][0])+ "." )

	
	if 'country' in freebaseAttributes.keys() and len(freebaseAttributes['country'])>0:
			sentences.append("It is a movie of " + str(freebaseAttributes['country'][0])+".")
	
	if 'directed by' in freebaseAttributes.keys():
		if len(freebaseAttributes['directed by'])>1:
			sentences.append("The movie is directed by " + freebaseAttributes['directed by'][0]+" and " + freebaseAttributes['directed by'][1]+".")
		else:
			sentences.append("The director of this movie is " + freebaseAttributes['directed by'][0]+".")
	
	if 'produced by' in freebaseAttributes.keys() and len(freebaseAttributes['produced by'])>0:
		if len(freebaseAttributes['produced by'])>1:
			sentences.append("The producers of the movie are " + freebaseAttributes['produced by'][0]+" and " + freebaseAttributes['produced by'][1]+".")
		else:
			sentences.append("The producer of the movie is "+ freebaseAttributes['produced by'][0]+".")

	if 'written by' in freebaseAttributes.keys():
		if len(freebaseAttributes['written by'])>1:
			sentences.append("The script writers of the movie are " + freebaseAttributes['written by'][0]+" and " + freebaseAttributes['written by'][1]+".")
		else:
			sentences.append("The script writer of the movie is "+ freebaseAttributes['written by'][0]+".")
	
	if 'release date' in freebaseAttributes.keys():
		sentences.append("The release date of the movie is " + freebaseAttributes['release date'][0])

	if 'notable for' in freebaseAttributes.keys():
		if len(freebaseAttributes['notable for'])>1:
			sentences.append("The movie is notable for " + freebaseAttributes['notable for'][0]+" and " + freebaseAttributes['notable for'][1]+".")
		else:
			sentences.append("The movie is notable for "+ freebaseAttributes['notable for'][0]+".")
	
	if 'genre' in freebaseAttributes.keys() and len(freebaseAttributes['genre'])>0:
		if len(freebaseAttributes['genre'])>1:
			sentences.append("Let me give you more clues... It is a " + freebaseAttributes['genre'][0]+" and " + freebaseAttributes['genre'][1]+".")
		else:
			sentences.append("Let me give you more clues... It is a "+ freebaseAttributes['genre'][0]+".")
			
	if 'taglines' in freebaseAttributes.keys() and len(freebaseAttributes['taglines'])>0:
		if len(freebaseAttributes['taglines'])>1:
			sentences.append("How about guessing it from its taglines? " + freebaseAttributes['taglines'][0]+" and " + freebaseAttributes['taglines'][1]+".")
		else:
			sentences.append("How about guessing it from its taglines? "+ freebaseAttributes['taglines'][0]+".")
	
	if 'sequel' in freebaseAttributes.keys() and len(freebaseAttributes['sequel'])>0:
			sentences.append("Well, it also has sequels..." )
	return sentences
	
	
def generateTemplateForLocation(freebaseData,wordnetData,word,category):
	sentences = list()
	freebaseAttributes = freebaseData['attributes']
	if 'SYN' in freebaseAttributes.keys():
		synonyms = filter_values(freebaseAttributes['SYN'],word,category)
		if len(synonyms) > 0:
			if len(synonyms) > 1:
				sentences.append("It is also known as " + synonyms[0] + " or "  + synonyms[1]+".")
			else:
				sentences.append("It is also called " + synonyms[0]+".")
	if wordnetData != None:
		wordnetAttrs = wordnetData['attributes']
		hypernyms = filter_values(wordnetAttrs['hypernyms'],word,category)
		if len(hypernyms)>0:
			
			sentences.append("It is a "+ hypernyms[len(hypernyms)-2] + ".")


		holonyms = filter_values(wordnetAttrs['holonyms'],word,category)
		
		if len(holonyms)>0:
			if len(holonyms)>1:
				sentences.append("It is in "+ holonyms[1] + ".")
				sentences.append("Let me make it more precise for you! " + "It is in " + holonyms[0]+".")
			else:
				sentences.append("It is in " + holonyms[0]+".")
		
		
		meronyms = filter_values(wordnetAttrs['meronyms'],word,category)
		if len(meronyms)>3:
			if len(meronyms)==3:
				sentences.append("It has these locations in it - " + meronyms[0] + ", "+ meronyms[1]+ " and "+ meronyms[2]+ ".")
			else:
				sentences.append("It has these locations in it - " + meronyms[0] + ", "+ meronyms[1]+ " and "+ meronyms[2]+ ".")
				length = len(meronyms)
				sent = "Let me give you some more locations in it. It consists of places such as "
				
				if length == 4:
					sent = sent +meronyms[length-1] + "."
				elif length == 5:
					sent = sent + meronyms[3]+ " and " +meronyms[4]+"."
				elif length >5:
					sent = sent + meronyms[3]+ ", "+ meronyms[4]+ " and "+ meronyms[5]+"."
				sentences.append(sent)
		else:
			sent = "It has these locations in it - "
			length = len(meronyms)
			if length == 1:
				sent = sent + meronyms[0]+"."
				sentences.append(sent)
			elif length == 2:
				sent = sent + meronyms[0]+ " and " +meronyms[1]+"."
				sentences.append(sent)
	if "capital of" in freebaseAttributes.keys():
			sentences.append("It is a capital of " + freebaseAttributes['capital of']+".")
	
	if wordnetData != None and "related_forms" in wordnetAttrs.keys():
		related_forms = filter_values(wordnetAttrs['related_forms'],word,category)
		if len(related_forms)>0:
			sentences.append("Now you should be able to guess it easily. "+ "The inhabitants of this place are known as "+ related_forms[0])

	return sentences

def generateTemplate(category,word):


	freebaseDirName = "../Data/freebase/"+category+"s"
	freebaseFilename = word+"_freebase.json"
	freebaseJsonfile = os.path.join(freebaseDirName,freebaseFilename)
	freebaseFile = open(freebaseJsonfile,'r')
	freebaseData = dict()
	freebaseData = json.load(freebaseFile)
	freebaseFile.close()

	try:
		wordnetDirName = "../Data/wordnet/"+category+"s"
		wordnetFilename = word+"_wordnet.json"
		wordnetJsonfile = os.path.join(wordnetDirName,wordnetFilename)
		wordnetFile = open(wordnetJsonfile,'r')
		wordnetData = dict()
		wordnetData = json.load(wordnetFile)
		wordnetFile.close()
	except:
		wordnetData = None
	
	if category == 'animal':
		sentences = generateTemplateForAnimal(freebaseData,wordnetData,word,category)
	if category == 'sport':
		sentences = generateTemplateForSport(freebaseData,wordnetData,word,category)
	if category == 'location':
		sentences = generateTemplateForLocation(freebaseData,wordnetData,word,category)
	if category == 'film':
		sentences = generateTemplateForFilm(freebaseData,None,word,category)
	outputDir = "../Data/Clues/"+category+"s"
	outputFile = word+"_attr_clues.txt"
	outputTextFile = os.path.join(outputDir,outputFile)
	output = open(outputTextFile,'w')
	for sent in sentences:
		output.write(sent + "\n")


def getCluesFromAttributes(category):
	dirName = "../Data"
	fileName = category+"s"+"_list.json"
	path = os.path.join(dirName,fileName)
	data = open(path,'r')
	wordsDict = dict()
	wordsDict = json.load(data)
	data.close()
	words = wordsDict[category+"s"]
	for word in words:
		generateTemplate(category,word)

getCluesFromAttributes('location')
getCluesFromAttributes('film')
getCluesFromAttributes('sport')
getCluesFromAttributes('animal')

