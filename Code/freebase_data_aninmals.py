import json
import urllib
import requests
import os
service_url = 'https://www.googleapis.com/freebase/v1/topic'
topic_id = '/m/0d6lp'
params = {
  'key': 'AIzaSyB8GpL8gwAlHLo9YIO4_ZDx2-PrqdIH1BA',
  'filter': '/common/topic'
}


query = json.dumps( [{'id': None, 'mid':None,'name': None,'type': '/biology/animal'}])
topic = requests.get('https://www.googleapis.com/freebase/v1/mqlread?query=%s' % query )
i=0
collection = list()
for rslt in topic.json()['result']:
	name = rslt['name']
	synList = list()

	collection.append(name)
	eachWordInfo = dict()
	print rslt['name'] + " &&& " + rslt['mid']
	mid = rslt['mid']
	eachWordInfo['word'] = name
	eachWordInfo['category'] = 'animal'
	eachWordInfo['attributes'] = dict()
	query1 = json.dumps( [{"master_property": "/biology/organism_classification/higher_classification","source": None,"target": {"mid":mid},"type": "/type/link"}])
	scientificNameRslt = requests.get('https://www.googleapis.com/freebase/v1/mqlread?query=%s' % query1 )
	if 'result' in scientificNameRslt.json().keys() and len(scientificNameRslt.json()['result']) > 0:
		allScientificNames = list()
		scientificNames = scientificNameRslt.json()['result']#[0]['source']
		print scientificNames
		for rslt in scientificNames:
			allScientificNames.append(rslt['source'])
		eachWordInfo['attributes']['scientific names'] = allScientificNames

	query2 = json.dumps( [{"master_property": "/fictional_universe/fictional_character/species","source": None,"target": {"mid":mid},"type": "/type/link"}])
	fictionaCharacterRslt = requests.get('https://www.googleapis.com/freebase/v1/mqlread?query=%s' % query2 )
	if 'result' in fictionaCharacterRslt.json().keys() and len(fictionaCharacterRslt.json()['result']) > 0:
		allFictionalCharacters = list()
		fictionalCharacterNames = fictionaCharacterRslt.json()['result']#[0]['source']
		print fictionalCharacterNames
		for rslt in fictionalCharacterNames:
			allFictionalCharacters.append(rslt['source'])
		eachWordInfo['attributes']['fictional character'] = allFictionalCharacters
	url = service_url + mid + '?' + urllib.urlencode(params)
	prop = json.loads(urllib.urlopen(url).read())['property']
	
	# print prop
	if '/common/topic/description' in prop.keys():
		data = prop['/common/topic/description']['values'][0]['value']
		eachWordInfo['description'] = data
	
	eachWordInfo['URL'] = 'https://www.freebase.com'+mid
	if '/common/topic/alias' in prop.keys():
		alias = prop['/common/topic/alias']['values']#[0]['text']
		allAlias = list()
		for rslt1 in alias:
			allAlias.append(rslt1['text'])
		eachWordInfo['attributes']['SYN'] = allAlias
	i+=1
	dirName = "../Data/freebase/animals"
	filename = name+"_freebase.json"
	jsonfile = os.path.join(dirName,filename)
	

	import json

	with open(jsonfile, 'w') as outfile:
    		json.dump(eachWordInfo, outfile) 

animalsDict = dict()
animalsDict['animals'] = collection

dirName = "../Data"
filename = "animals_list.json"
jsonfile = os.path.join(dirName,filename)
import json
with open(jsonfile,'w') as outfile:
	json.dump(animalsDict,outfile)

#print( '------------------------------')




