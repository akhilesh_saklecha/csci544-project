from nltk.corpus import wordnet as wn
import os
import json

dirName = "../Data"
filename1 = "locations_list.json"
jsonfile1 = os.path.join(dirName,filename1)
filename2 = "sports_list.json"
jsonfile2 = os.path.join(dirName,filename2)
filename3 = "animals_list.json"
jsonfile3 = os.path.join(dirName,filename3)

sportsFile = open(jsonfile2,'r')
sports = dict()
sports = json.load(sportsFile)
sportsFile.close()
locationsFile = open(jsonfile1,'r')
locations = dict()
locations = json.load(locationsFile)
locationsFile.close()
animalsFile = open(jsonfile3,'r')
animals = dict()
animals = json.load(animalsFile)
animalsFile.close()
def findInfoFromWordnet(words,fileName):
	i=0

	eachWordInfo = dict()
	for word in words:
		eachWordInfo['word']=word
		eachWordInfo['attributes']=dict()
		category = fileName.split("_")[0]
		eachWordInfo['category'] = category[:len(category)-1]
		wordForWordnet = word.replace(' ','_')
		try:
			wordSynset = wn.synset(wordForWordnet+'.n.01')
		
		except:
			continue

		#related forms of words
		relatedFormsList = list()
		relatedForms = set([lemma for lemma in wordSynset.lemmas()])
		for lemma in relatedForms:
			for lemma1 in lemma.derivationally_related_forms():

				relatedFormsList.append(lemma1.name().split(".")[0].replace("_"," "))
				break
			# relatedFormsList.append(lemma.name().split(".")[0].replace('_',' '))
		eachWordInfo['attributes']['related_forms'] = relatedFormsList
		# print(wordSynset)
		#synonyms of words
		synonyms = list()
		for synset in wn.synsets(wordForWordnet):
			synonyms.append(synset.name().split(".")[0].replace('_',' '))	
		eachWordInfo['attributes']['SYN']=synonyms
		
		#description of the words
		eachWordInfo['description'] = wn.synset(wordForWordnet+".n.01").definition()

		
		
		#hypernyms of words
		hypernymsList = list()
		hypernyms = wn.synset(wordForWordnet+".n.01").hypernym_paths()
		for synset in hypernyms[0]:
			hypernymsList.append(synset.name().split(".")[0].replace('_',' '))
		eachWordInfo['attributes']['hypernyms']=hypernymsList[len(hypernymsList)-4:]
		
		#hyponyms of words
		hyponymsList = list()
		hyponyms = set([i for i in wordSynset.closure(lambda s:s.hyponyms())])
		for synset in hyponyms:
			hyponymsList.append(synset.name().split(".")[0].replace('_',' '))
		eachWordInfo['attributes']['hyponyms']=hyponymsList

		#meronyms of words
		meronymsList = list()
		meronyms = set([i for i in wordSynset.closure(lambda s:s.part_meronyms())])
		for synset in meronyms:
			meronymsList.append(synset.name().split(".")[0].replace('_',' '))
		eachWordInfo['attributes']['meronyms']=meronymsList

		#holonyms of words
		holonymsList = list()
		holonyms = set([i for i in wordSynset.closure(lambda s:s.part_holonyms())])
		for synset in holonyms:
			holonymsList.append(synset.name().split(".")[0].replace('_',' '))
		eachWordInfo['attributes']['holonyms']=holonymsList

		#domain terms of words
		domainTermsList = list()
		domainTerms = set([i for i in wordSynset.closure(lambda s:s.similar_tos())])
		for synset in domainTerms:
			domainTermsList.append(synset.name().split(".")[0].replace('_',' '))
		eachWordInfo['attributes']['domainTerms']=domainTermsList
		
		i+=1
		dirName = "../Data/wordnet/"+fileName.split("_")[0]
		filename = word+"_wordnet.json"
		jsonfile = os.path.join(dirName,filename)
		

		import json

		with open(jsonfile, 'w') as outfile:
	    		json.dump(eachWordInfo, outfile)
	# print(eachWordInfo)
findInfoFromWordnet(locations['locations'],filename1)
findInfoFromWordnet(sports['sports'],filename2)
findInfoFromWordnet(animals['animals'],filename3)


#initial practice

# print(fb.hypernyms())
# print(fb.hyponyms())
# print(fb.member_holonyms())
# print(fb.root_hypernyms()[0].name)
# print(wn.morphy('dogs'))
# print(wn.morphy('rotating'))
# print(wn.morphy('happiness'))
# print(wn.synset('football.n.01').lowest_common_hypernyms(wn.synset('cricket.n.01')))
# for lemma in wn.lemmas('football', 'n'):
#      print(lemma, lemma.count())
# print("------------------------")
# dog = wn.synset('North_America.n.01')
# cat = wn.synset(wn.morphy('territories')+'.n.01')
# print(dog.path_similarity(cat))  

# hyperapple = set([i for i in fb.closure(lambda s:s.hypernyms())])
# hypoapple = set([i for i in fb.closure(lambda s:s.hyponyms())])
# # print(hyperapple[1][0])
# for synset in hyperapple:
# 	print(synset.name().split(".")[0].replace('_',' '))
# print("dfdddg")
# for synset in hypoapple:
# 	print(synset.name().split(".")[0].replace('_',' '))
# for synset in syn:
# 	print(synset.name().split(".")[0].replace('_',' '))

