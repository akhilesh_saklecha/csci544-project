import os
import requests
import xml.etree.ElementTree as ET
import unicodedata
from collections import defaultdict
import pdb
import json

def getDescription(word):
	url = 'http://lookup.dbpedia.org/api/search.asmx/KeywordSearch'
	parameters = defaultdict()
	parameters['QueryString'] = word

	r = requests.get(url,params=parameters)

	description = None
	wordURL = None

	if(r.status_code == 200):
		xml = ET.fromstring(r.text)
		#pdb.set_trace()
		for result in xml:
			if(result[0].text.lower() == word.lower()):
				try:
					wordURL = result[1].text
					description = unicodedata.normalize("NFKD",result[2].text)
					#print(wordURL)
					print(description)
				except:
					pass #word not found in wikipedia

	if(description):
		return (wordURL,description)
	else:
		return (None,None)

def main():

	f = open('sports_list.json')

	jsonKey = 'sports'
	jsonFile = f
	jsonData = json.load(jsonFile)
	os.chdir('sports')
	for elem in jsonData[jsonKey]:
		data = defaultdict()
		URL,description = getDescription(elem)
		if(description):
			data['URL'] = URL
			data['description'] = description
			data['word'] = elem
			outFile = open(elem+ '_dbpedia.json','w')
			json.dump(data,outFile)
			outFile.close()

if __name__ == '__main__':
	main()