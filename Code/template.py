import os
import extractNP
import rules
from collections import Counter
import pdb
from nltk.parse import stanford


class template():
	def __init__(self):
		self.grammar = Counter()
		verbs = ['is','has']
		prepositions = ['in','of','at']
		conjunction = ['and']
		self.joiners = [verbs,prepositions,conjunction]
		self.np = extractNP.init()
		self.initializeGrammar()
		self.generateTemplate()

	def initializeGrammar(self):
		try:
			grammarFile = open('rules.txt')
			for line in grammarFile:
				key = line.split('->')[0]
				value = int(line.split('->')[1])
				self.grammar[key] = value
		except:
			print("error in reading grammar file")


	def generateTemplate(self):
		
		
		for j in range(0,len(self.np)):
			i = 0
			#pdb.set_trace()
			sentence = 'It'
			while(i < len(self.np[j])):
				mostProbableSentence = ''
				maxProbability = 0
				p = 0
				
				np = ''
				for x in self.np[j][i]:
					np += x +' '
				np = np[:-1]

				#sentence += ' '+ 'joiner'+ ' '+ np
				for k in range(0,3):
					testFragment = ' '+ self.joiners[k][0]+ ' '+ np
					testSentence = sentence + testFragment
					p = self.checkProbability(testSentence)
					if(p > maxProbability):
						maxProbability = p
						mostProbableSentence = testSentence
					#pdb.set_trace()
				#pdb.set_trace()
				if(len(mostProbableSentence) > 1 ):
					sentence = mostProbableSentence
				i += 1
			print(sentence)
			

	def checkProbability(self,sentence):
		os.chdir("../")
		parser = stanford.StanfordParser(model_path='/media/akhilesh/Akhilesh/Study/USCAssignments/NLP/project/stanford-parser-full-2015-01-30/stanford-parser-3.5.1-models/edu/stanford/nlp/models/lexparser/englishPCFG.ser.gz')
		tree = parser.raw_parse(sentence)
		sentenceGrammar = Counter()
		rules.extractRules(tree[0][0],sentenceGrammar)
		p = 1
		#pdb.set_trace()
		for key in sentenceGrammar.keys():
			if(self.grammar[key] != 0):
				p *= self.grammar[key]
		return p

def main():
	templateInstance = template()


if __name__ == '__main__':
	main()