﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Net;
using System.Text.RegularExpressions;
using System.Xml;


namespace ThousandPyramids
{
    public partial class _Default : System.Web.UI.Page
    {
        
        protected void Page_Load(object sender, EventArgs e)
        {
            

        }

        string[] okay = { "You are doing okay!! Think about it!!", "hmm.. Think more. Believe in yourself! Have faith in your abilities", "Not bad.. Try Harder. You can do it", "Ahh.. Try again!! Never give up, for that is just the place and time that the tide will turn" };
        string[] right = { "You are going in the right direction. A creative person is motivated by the desire to achieve..", "Nops!!  It does not matter how slowly you go as long as you do not stop.!!","" };
        public string[] getNewData()
        {

            string[] directories = Directory.GetDirectories(Server.MapPath("Attributes"));

            if (ViewState["LastType"] == null )
            {
                ViewState["LastType"] = directories[0];
            }
            else if (ViewState["LastType"].ToString() == directories[0])
            {
                ViewState["LastType"] = directories[1];
            }
            else if (ViewState["LastType"].ToString() == directories[1])
            {
                ViewState["LastType"] = directories[2];
            }
            else if (ViewState["LastType"].ToString() == directories[2])
            {
                ViewState["LastType"] = directories[3];
            }
            else if (ViewState["LastType"].ToString() == directories[3])
            {
                ViewState["LastType"] = directories[0];
            }
           

            string[] files = Directory.GetFiles(ViewState["LastType"].ToString());
            Random r = new Random();
            int index = r.Next(0, files.Length);

            string[] dataAttr = File.ReadAllLines(files[index]);
            FileInfo fi = new FileInfo(files[index]);
            ViewState["word"] = fi.Name.Replace("_attr_clues.txt","");
            return dataAttr;
        }

        public void getExistingData()
        {
            if (ViewState["index"] == null)
            {
                string[] data = getNewData();
                ViewState["CurrentData"] = data;
                ViewState["index"] = 0;
                if (ViewState["PreviousText"] == null)
                    ViewState["PreviousText"] = "";
                ViewState["PreviousText"] = ViewState["PreviousText"]+ "<b>System : </b>" + data[0] + "<br/>";
                InteractionLabel.Text = ViewState["PreviousText"].ToString();

            }
            else
            {
                string[] data =  (string[])ViewState["CurrentData"];
                int index=  Convert.ToInt32(ViewState["index"]);
                if(index<data.Length-1)
                    ViewState["PreviousText"] = ViewState["PreviousText"] + "<b>System : </b>" + data[index + 1] + "<br/>";
                else
                {
                    ViewState["PreviousText"] = ViewState["PreviousText"] + "<b>System : </b>" + "Sorry all clues already given!! Guess the word Now!!" + "<br/>";
                }
                ViewState["index"]=index+1;
                InteractionLabel.Text = ViewState["PreviousText"].ToString();

            }

        }

     
        int getRandomNumber(int length){
            Random r = new Random();
            int index = r.Next(0, length);
            return index;

        }
        public string getSimilarity()
        {
            string url = string.Format("http://www.mechanicalcinderella.com/index.php?inset%5B%5D={0}&inset%5B%5D={1}&inset%5B%5D=&inset%5B%5D=&inset%5B%5D=&inatr%5B%5D=&inatr%5B%5D=&inatr%5B%5D=&inatr%5B%5D=&inatr%5B%5D=&domena=#results", ViewState["word"], TextBox_guess.Text);
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            HttpWebResponse response = (HttpWebResponse)request.GetResponse();
            Stream receiveStream = response.GetResponseStream();
            StreamReader readStream = new StreamReader(receiveStream);
            String similrity = readStream.ReadToEnd();
            Regex regex = new Regex(@"hlevel.*>(0-9)*<");
            Match match = regex.Match(similrity);
            if (match.Success)
            {
                regex = new Regex(@"\d+.\d+");
                match = regex.Match(match.Value);
                if (match.Success)
                    similrity = match.Value;
            }
           
            return similrity;

        }
        
        protected void Button_Check_Click(object sender, EventArgs e)
        {
            if (InteractionLabel.Text == string.Empty || ViewState["index"] ==null)
            {
                ViewState["PreviousText"] = "<b>System : </b>" + "Strange!! You are guessing without even getting your first clue. Too Smart!!" + "<br/>";
                InteractionLabel.Text = ViewState["PreviousText"].ToString();
            }
            else
            {
                ViewState["PreviousText"] = ViewState["PreviousText"] + "<b>User : </b>" + "You guessed <i>" + TextBox_guess.Text + "</i><br/>";
                InteractionLabel.Text = ViewState["PreviousText"].ToString();
                try
                {

                    string sim = getSimilarity();
                    if (ViewState["word"].ToString().ToLower().Trim() == TextBox_guess.Text.ToLower().Trim())
                    {
                        ViewState["PreviousText"] = ViewState["PreviousText"] + "<b>System : </b>" + "Perfect!! Your guess is correct.<br/>";
                        InteractionLabel.Text = ViewState["PreviousText"].ToString();
                    }
                    else if (Convert.ToDouble(sim) < -100)
                    {
                        ViewState["PreviousText"] = ViewState["PreviousText"] + "<b>System : </b>" + "Sorry, but your guess is wrong.<br/>";
                        InteractionLabel.Text = ViewState["PreviousText"].ToString();
                    }


                    else if (Convert.ToDouble(sim) >= 0.7)
                    {
                        ViewState["PreviousText"] = ViewState["PreviousText"] + "<b>System : </b>" + "Your guess is damn close. Think more !! You can crack.<br/>";
                        InteractionLabel.Text = ViewState["PreviousText"].ToString();
                    }
                    else if (Convert.ToDouble(sim) >= 0.5)
                    {
                        ViewState["PreviousText"] = ViewState["PreviousText"] + "<b>System : </b>" + "You are going in right direction!!" + "<br/>";
                        InteractionLabel.Text = ViewState["PreviousText"].ToString();
                    }
                    else if (Convert.ToDouble(sim) >= 0.2)
                    {
                        ViewState["PreviousText"] = ViewState["PreviousText"] + "<b>System : </b>" + okay[getRandomNumber(okay.Length)] + "<br/>";
                        InteractionLabel.Text = ViewState["PreviousText"].ToString();
                    }
                    else if (Convert.ToDouble(sim) >= 0.1)
                    {
                        ViewState["PreviousText"] = ViewState["PreviousText"] + "<b>System : </b>" + "You are heading in wrong direction. Think wisely.!! <br/>";
                        InteractionLabel.Text = ViewState["PreviousText"].ToString();
                    }
                }
                catch (Exception ex)
                {

                    ViewState["PreviousText"] = ViewState["PreviousText"] + "<b>User : </b>" + "Sorry, but your guess is wrong.<br/>";
                    InteractionLabel.Text = ViewState["PreviousText"].ToString();
                }
            }
        }

        protected void Button_NextClue_Click(object sender, EventArgs e)
        {
            getExistingData();
        }

        protected void Button_NextWord(object sender, EventArgs e)
        {
            ViewState["PreviousText"] = null;
            ViewState["index"] = null;
            ViewState["word"] = null;
            InteractionLabel.Text = "";
            getExistingData();

        }
        
    }
}